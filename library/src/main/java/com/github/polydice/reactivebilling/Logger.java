package com.github.polydice.reactivebilling;

public interface Logger {
  void log(String message);

  Logger DEFAULT = new Logger() {
    @Override public void log(String message) {
      // no op
    }
  };
}
